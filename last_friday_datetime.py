import datetime
from calendar import monthrange


def get_last_friday(input_data):
    month = int(input_data.split('/')[0])
    year = int(input_data.split('/')[1])
    number_of_days = monthrange(year, month)[1]
    input_data_str = f'01.{month}.{year}'
    days_in_month = []
    day_iter = input_data_str.split('.')
    for day in range(number_of_days):
        days_in_month.append(f'{str(int(day_iter[0]) + day).zfill(2)}.{day_iter[1].zfill(2)}.{day_iter[2].zfill(4)}')
    days_in_month_rev = days_in_month[::-1]
    days_list = []
    for day in days_in_month_rev:
        days_list.append(str((datetime.datetime.strptime(day, '%d.%m.%Y').weekday())+1))
    index = ''.join(days_list).find('5')
    return days_in_month_rev[index]
